<?php

use Illuminate\Database\Seeder;

class ChiTietHoaDonNhap_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chitiethoadonnhap')->insert([
            'MaHDNhap' => 'HD01',
            'MaSP'=>'SP01',
            'SoLuong' => 10,	
        ]);
    }
}
