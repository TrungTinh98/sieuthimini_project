<?php

use Illuminate\Database\Seeder;

class HoaDonBan_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hoadonban')->insert([
            'MaHDBan' => 'HDB01',
            'id'=>'215418557',
            'NgayLap' => '15/03/2019',
            'TongTien'=>2000000,	
        ]);    
    }
}
