<?php

use Illuminate\Database\Seeder;

class Users_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => '215418557',
            'user_name'=>'TrungTinh',
            'email' => 'thaitrungtinh98@gmail.com',
            'password' => bcrypt('12345678'),
            'level'=>1,
            'active'=>1,
            'image'=>'user.png',
            'type_accout'=>'Admin',
            'remember_token'=>null,
        ]);
    }
}
