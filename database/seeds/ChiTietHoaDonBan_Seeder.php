<?php

use Illuminate\Database\Seeder;

class ChiTietHoaDonBan_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chitiethoadonban')->insert([
            'MaHDBan' => 'HDB01',
            'MaSP'=>'SP01',
            'SoLuong' => 10,	
        ]);
    }
}
