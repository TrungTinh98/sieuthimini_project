<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNhaphanphoiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nhaphanphoi', function (Blueprint $table) {
            $table->string('MaNPP');
            $table->string('TenNPP');
            $table->string('DiaChi');
            $table->string('SDT');
            $table->string('Email');
            $table->string('ThongTin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nhaphanphoi');
    }
}
