<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSanphamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sanpham', function (Blueprint $table) {
            $table->string('MaSP');
            $table->string('TenSP');
            $table->integer('SoLuong');
            $table->float('DonGia');
            $table->string('Image');
            $table->string('MaLoaiSP');
            $table->string('MaNPP');
            $table->float('KhuyenMai');
            $table->integer('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sanpham');
    }
}
