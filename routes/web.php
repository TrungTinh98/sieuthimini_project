<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('dang-ky', 'Auth\RegisterController@showRegisterForm');
// Route::post('doi-mat-khau',['as' => 'resetpassword', 'uses' => 'Auth\PasswordRetrievaController@postDoiMatKhau']);
Route::post('doi-mat-khau','Auth\PasswordRetrievaController@postDoiMatKhau');
Route::get('logout', 'Auth\LoginController@logout');

Route::get('loai-san-pham','LoaiSanPhamController@getLoaiSanPham');


Route::get('san-pham','SanPhamController@getSanPham');


Route::get('hoa-don-nhap','HoaDonController@getHoaDonNhap');

Route::get('hoa-don-ban','HoaDonController@getHoaDonBan');

Route::get('nha-phan-phoi','NhaPhanPhoiController@getNhaPhanPhoi');

Route::get('thong-ke-bao-cao','ThongKeBaoCaoController@getThongKeBaoCao');

Route::get('quan-ly-nhan-vien','QuanLyNhanVienConTroller@getNhanVien');

Route::get('chi-tiet-nhan-vien','QuanLyNhanVienConTroller@getChiTietNhanVien');

Route::get('quan-ly-to-chuc-cong-ty','QuanLyNhanVienConTroller@getQuanLyToChuc');

Route::get('quan-ly-loai-nhan-vien','QuanLyNhanVienConTroller@getQuanLyLoaiNhanVien');