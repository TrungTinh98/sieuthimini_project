<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";
    public $timestamps = false;
    protected $fillable = [
        'id',
        'user_name',
        'email',
        'email_verified_at',
        'password',
        'type_account',
        'active',
        'level',
        'message',
        'image',
        'remember_token',
        'created_at',
        'updated_at',
    ];
}
