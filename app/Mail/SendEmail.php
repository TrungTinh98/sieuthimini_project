<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $sub;
    public $mail;
    public $id;
    public function __construct($subject,$to_email,$to_id)
    {
        $this->sub=$subject;
        $this->mail=$to_email;
        $this->id=$to_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        //return $this->subject('Siêu Thị MiNi')->view("pages.SenMail");
        return $this->subject("Siêu Thị MiNi")->view('pages.SenMail')->with([
                        'token' => $this->sub,
                        'email'=>$this->mail,
                        'id'=>$this->id,
                    ]);
    }
}
