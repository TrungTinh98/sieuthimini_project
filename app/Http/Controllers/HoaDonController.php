<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HoaDonController extends Controller
{
    public function getHoaDonNhap()
    {
    	return view('pages.hoadonnhap');
    }

    public function getHoaDonBan()
    {
    	return view('pages.hoadonban');
    }
}
