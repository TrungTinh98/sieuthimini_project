<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuanLyNhanVienConTroller extends Controller
{
    public function getNhanVien()
    {
    	return view('pages.QuanLyNhanVien');
    }
    public function getQuanLyToChuc()
    {
    	return view('pages.quanlytochuccongty');
    }
    public function getQuanLyLoaiNhanVien()
    {
    	return view('pages.quanlyloainhanvien');
    }


    public function getChiTietNhanVien()
    {
    	return view('pages.ChiTietNhanVien');
    }
}
