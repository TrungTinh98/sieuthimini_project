<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use App\Mail\SendEmail;
use Mail;
use Validator;
use Hash;
use send;

class PasswordRetrievaController extends Controller
{
	public function postDoiMatKhau(Request $request)
	{


		$rules = [
			'email' => 'required|email',

		];	
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			$result["status"] = false;
            $result["message"] = "Email là trường bắt buộc!";
            return Response::json($result);
		 	//return redirect()->back()->withInput()->withErrors(['error', 'Email là trường bắt buộc']);
		} 
		else {
			$email_input = $request->input('email');//lấy email do người dùng nhập vào
			$check_email=User::where('email','like',$email_input)->get();//kiểm tra email do người dùng nhập vào so với database.
			if($check_email && count($check_email) > 0)
			{
					// ----------------------tạo random_token----------------------------------
				$token = "";
				$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				$codeAlphabet.= "0123456789";
				for($i=0;$i<20;$i++)
				{
					$token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
				}
			        // ----------------------Kết thúc phần tạo token-----------------------------

					// ----------------------chức năng update remember_token-----------------------------
					$pick_ID=User::where('email','like',$email_input)->value('id');//lấy ra id từ email người dùng. 
					User::where('id','=',$pick_ID)->update(array('remember_token'=>$token));//update đoạn mã vào database sau này lấy lại để gửi mail cho người dùng.
					// --------------------------------kết thúc lệnh update-------------------------------
				
					//----------------------------gửi email đến người dùng--------------------------------

					$to_name = User::where('id','=',$pick_ID)->value('user_name');// lấy tên của người dùng.
					$remember_token=User::where('id','=',$pick_ID)->value('remember_token');//lấy ra token đẻ gửi mail.
					$to_email = User::where('id','=',$pick_ID)->value('email');
					$password= User::where('id','=',$pick_ID)->value('password');
					
					$subject=$remember_token;
					$email=$to_email;
					$id=$pick_ID;


					Mail::to($to_email)->send(new SendEmail($subject,$email,$id));
					$result["status"] = false;
		            $result["message"] = "Đã xác nhận vui lòng kiểm tra mail!";
		            return Response::json($result);
					//------------------------------kết thúc lệnh gửi mail---------------------------------
			}
			else{
				$result["status"] = false;
	            $result["message"] = "Email không tồn tại!";
	            return Response::json($result);
			}
		}
	}
}
