<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    public function username()
    {
        return 'id';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function logout() {
    //logout user
        auth()->logout();
    // redirect to homepage
        return redirect('login');
    }

    public function showLoginForm()
    {
        return view('auth.login_sieuthi');  
    }

    protected function validateLogin(Request $request)
    {

       $rules = [
        $this->username() => 'required|string',
        'password' => 'required|string',
    ];
    $attributes = [
        $this->username() => 'ID',
        'password' => 'Mật khẩu',
    ];
    $customMessages = [
        'required' => ':attribute là trường bắt buộc!',
    ];
    $this->validate($request, $rules, $customMessages, $attributes);
}

protected function credentials(Request $request)
{
    $id=$request->{$this->username()};

    return ['id' => $id, 'password' => $request->password, 'active' => 1];
}
}
