@extends('parts.main')
@section('content')
<div class="right_col" role="main" style="min-height: 1723px;">

	<!-- Form đăng ký nhân viên mới -->
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><b>Đăng Ký Loại Tài Khoản</b></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate="">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Mã loại tài khoản <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">Tên loại tài khoản <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="occupation" type="text" name="occupation" class="optional form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">Tình trạng  <span class="required">*</span>
                        </label>
                        <!-- Default unchecked -->
                          <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
                              <label class="custom-control-label" for="defaultUnchecked">Hoạt động</label>
                          </div>
                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                      	<center>
                      		<div class="col-md-6 col-md-offset-3">
                          	<button type="submit" class="btn ">Hủy</button>
                          	<button id="send" type="submit" class="btn btn-success" >Đăng ký</button>
                            <button type="submit" class="btn btn-primary">Sửa</button>
                        </div>
                      	</center>
                        
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- kết thúc form đăng lý -->

            <!-- Danh sách loại nhân viên  -->
            <div class="row">
              <div class="clearfix"></div>
              <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><b>Danh sách loại nhân viên nhân viên</b></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  	<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>

                  <div class="x_content">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                            </th>
                            <th class="column-title">Mã Loại NV </th>
                            <th class="column-title">Tên Loại NV</th>
                            <th class="column-title"><center>Hoạt động </center></th>
                            <th class="column-title no-link last"><span class="nobr"></span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr class="even pointer">
                            <td class="a-center ">
                              <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                            </td>
                            <td class=" ">121000040</td>
                            <td class=" ">ABCXXXXXXXXXXXXXXX</td>
                            <td class="a-right a-right "><center>X</center></td>
                            <td>
                            <button href="{{url('chi-tiet-nhan-vien')}}" class="btn btn-primary btn-xs" style="width: 70px;"><i class="fa fa-eye"></i>Chi tiết</button>
                          </td>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- ------------------- -->
          </div>
        </div>
@endsection