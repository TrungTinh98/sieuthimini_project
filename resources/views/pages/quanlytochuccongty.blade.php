@extends('parts.main')
@section('content')
<div class="right_col" role="main" style="min-height: 1723px;">

	<!-- Form đăng ký nhân viên mới -->
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><b>Đăng ký bộ phận lãnh đạo</b></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate="">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Mã người lãnh đạo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="Số chứng minh nhân dân" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">Họ & Tên <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="occupation" type="text" name="occupation"  data-validate-length-range="5,20" class="optional form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" required="required"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Chức vụ <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input list="chucvu" type="dropdown" id="number" name="number" required="required"  class="form-control col-md-7 col-xs-12">
						  <datalist id="chucvu">
						    <option value="Cập nhật chức vụ">
						  </datalist>
                        </div>
                      </div>            
                      <div class="item form-group">
                        <label for="password" class="control-label col-md-3">Mật khẩu <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password" type="password" name="password" placeholder="Mật khẩu ít nhất 8 ký tự + ký tự đặt biệt" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Nhập lại mật khẩu <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password2" type="password" name="password2" data-validate-linked="password" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Số điện thoại <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="tel" id="telephone" name="phone"  required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Địa chỉ<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="tel" id="diachi" name="_diachi" required="required"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                      	<center>
                      		<div class="col-md-6 col-md-offset-3">
                          	<button type="submit" class="btn" >Hủy</button>
                          	<button type="submit" class="btn btn-primary" >Sửa</button>
                          	<button id="send" type="submit" class="btn btn-success" >Đăng ký</button>
                        </div>
                      	</center>
                        
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- kết thúc form đăng lý -->

          	<!-- Bộ phần quản lý  -->
          	<div class="row">
          		<!-- Bộ phận quản lý công ty -->
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><b>Bộ phận quản lý công ty</b></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">ID </th>
                            <th class="column-title">Họ & Tên</th>
                            <th class="column-title">Email</th>
                            <th class="column-title">Điện thoại </th>
                            <th class="column-title">Chức vụ </th>
                            <th class="column-title no-link last"><span class="nobr"></span></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="even pointer">
                            <td class=" ">121000040</td>
                            <td class=" ">ABCXXXXXXXXXXXXXXX</td>
                            <td class=" ">ABCXXXXXXXXXXXXXXXXXXXX@gmail.com </td>
                            <td class=" ">097XXXXXXX</td>
                            <td class="a-right a-right ">Giám Đốc</td>
                            <td>
                            <a href="{{url('chi-tiet-nhan-vien')}}" class="btn btn-primary btn-xs" style="width: 70px;"><i class="fa fa-eye"></i> Chi tiết </a>
                          </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- kết thúc form bộ phận quản ly công ty -->


			<!-- Loại tài khoản -->
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><b>Bộ phận quản lý hệ thống</b></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">ID </th>
                            <th class="column-title">Họ & Tên</th>
                            <th class="column-title">Email</th>
                            <th class="column-title">Điện thoại </th>
                            <th class="column-title">Chức vụ </th>
                            <th class="column-title no-link last"><span class="nobr"></span></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="even pointer">
                            <td class=" ">121000040</td>
                            <td class=" ">ABCXXXXXXXXXXXXXXX</td>
                            <td class=" ">ABCXXXXXXXXXXXXXXXXXXXX@gmail.com </td>
                            <td class=" ">097XXXXXXX</td>
                            <td class="a-right a-right ">Trưởng phòng</td>
                            <td>
                            <a href="{{url('chi-tiet-nhan-vien')}}" class="btn btn-primary btn-xs" style="width: 70px;"><i class="fa fa-eye"></i> Chi tiết </a>
                          </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Kết thúc bộ phận quản lý hệ thống -->
            <!-- ----------------- -->
          </div>
        </div>
@endsection
