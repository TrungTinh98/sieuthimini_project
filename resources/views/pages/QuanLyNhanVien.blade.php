@extends('parts.main')
@section('content')
<div class="right_col" role="main" style="min-height: 1723px;">

	<!-- Form đăng ký nhân viên mới -->
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><b>Đăng ký nhân viên</b></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate="">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Mã Nhân viên <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="Số chứng minh nhân dân" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">Họ & Tên <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="occupation" type="text" name="occupation"  data-validate-length-range="5,20" class="optional form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" required="required"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Khu vực là việc <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input list="browsers" type="dropdown" id="number" name="number" required="required"  class="form-control col-md-7 col-xs-12">
            						  <datalist id="browsers">
            						    <option value="Cập nhật khu vực">
            						  </datalist>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Chức vụ <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input list="chucvu" type="dropdown" id="number" name="number" required="required"  class="form-control col-md-7 col-xs-12">
						  <datalist id="chucvu">
						    <option value="Cập nhật chức vụ">
						  </datalist>
                        </div>
                      </div>            
                      <div class="item form-group">
                        <label for="password" class="control-label col-md-3">Mật khẩu <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password" type="password" name="password" placeholder="Mật khẩu ít nhất 8 ký tự + ký tự đặt biệt" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Nhập lại mật khẩu <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password2" type="password" name="password2" data-validate-linked="password" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Số điện thoại <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="tel" id="telephone" name="phone"  required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Địa chỉ<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="tel" id="diachi" name="_diachi" required="required"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                      	<center>
                      		<div class="col-md-6 col-md-offset-3">
                          	<button type="submit" class="btn" >Hủy</button>
                          	<button id="send" type="submit" class="btn btn-success" >Đăng ký</button>
                            <button type="submit" class="btn btn-primary">Sửa</button>
                        </div>
                      	</center>
                        
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- kết thúc form đăng lý -->

          	<!-- Bộ phần quản lý  -->
          	<div class="row">
          		


			

            <!-- Danh sách nhân viên -->
            <div class="row">
              <div class="clearfix"></div>
              <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><b>Danh sách nhân viên</b></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  	<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>

                  <div class="x_content">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                            </th>
                            <th class="column-title">Mã NV </th>
                            <th class="column-title">Họ tên NV </th>
                            <th class="column-title">Email</th>
                            <th class="column-title">Ngày sinh </th>
                            <th class="column-title">Điện thoại </th>
                            <th class="column-title">Chức vụ </th>
                            <th class="column-title no-link last"><span class="nobr"></span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr class="even pointer">
                            <td class="a-center ">
                              <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                            </td>
                            <td class=" ">121000040</td>
                            <td class=" ">ABCXXXXXXXXXXXXXXX</td>
                            <td class=" ">ABCXXXXXXXXXXXXXXXXXXXX@gmail.com </td>
                            <td class=" ">John Blank L</td>
                            <td class=" ">097XXXXXXX</td>
                            <td class="a-right a-right ">Giám Đốc</td>
                            <!-- <td class=" last"><a href="#">View</a> -->
                            <td>
                            <a href="{{url('chi-tiet-nhan-vien')}}" class="btn btn-primary btn-xs" style="width: 70px;"><i class="fa fa-eye"></i></i> Chi tiết </a>
                           <!--  <a href="#" class="btn btn-danger btn-xs" style="width: 70px;"><i class="fa fa-trash-o"></i> Xóa </a> -->
                            <button class="btn btn-success btn-xs" style="width: 70px;"><i class="fa fa-search-plus"></i></i> chọn </button>
                          </td>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- ------------------- -->
          </div>
        </div>
@endsection