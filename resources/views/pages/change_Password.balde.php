
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Siêu Thị MiNi | Quên mật khẩu</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="admin_layouts/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="admin_layouts/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="admin_layouts/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="admin_layouts/adminlte/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="admin_layouts/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .login-box-body{
            padding-bottom: 50px!important;
        }
    </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <!-- <a href="https://vietvang.net/"><b>Viet Vang</b>JSC</a> -->
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg" style="font-size: 25px;color: #0a7ae0">Quên mật khẩu</p>
        <p class="login-box-msg">

        </p>
        <form action="{{route('UpdatePassword')}}" method="post">
            <meta name="csrf-token" content="{{ csrf_token() }}" />
            <div class="form-group has-feedback">
                <input name="new_password" type="password" class="form-control" placeholder="Mật khẩu mới" >
                @if($errors->has('new_password'))
                <p style="color:red">{{$errors->first('new_password')}}</p>
                @endif
                <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
            </div>
            <div class="form-group has-feedback">
                <input  name="confirm_password" type="password" class="form-control" placeholder="Xác nhận mật khẩu mới">
                @if($errors->has('confirm_password'))
                <p style="color:red">{{$errors->first('confirm_password')}}</p>
                @endif
                <!-- <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <input name="email" type="hidden" value="{{ $_GET['email'] }}" required="required"><!-- gán giá trọ email từ đường dẫn về (hay) -->
                    <input name="id" type="hidden" value="{{ $_GET['id'] }}" required="required"><!-- gán giá trọ id từ đường dẫn về (hay) -->
                    <input name="token" type="hidden" value="{{ $_GET['token'] }}" required="required"><!-- gán giá trọ id từ đường dẫn về (hay) -->
                    <button type="submit" name="dang_nhap" value="dang_nhap" class="btn btn-primary btn-block btn-flat">Cập nhập</button>
                
                  {!!csrf_field()!!}
                </div>
                <!-- /.col -->
            </div>
        </form>
        
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
