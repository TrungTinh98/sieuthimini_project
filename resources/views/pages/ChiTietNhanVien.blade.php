@extends('parts.main')
@section('content')
<div class="right_col" role="main" style="min-height: 2421px;">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Thông tin nhân viên</h3>
              </div>

              </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="images/picture.jpg" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>Tên Nhân Viên XXXXXXXXXXXXX </h3>
                      <ul class="list-unstyled user_data">
                        <li>
                          <i class="fa fa-key"> Mã NV: </i>
                        </li>
                        <li>
                          <i class="fa fa-map-marker user-profile-icon">Địa chỉ:</i> 
                        </li>
                        
                        <li>
                          <i class="fa fa-briefcase user-profile-icon"> Chức vụ: </i>
                        </li>
                        <li>
                          <i class="fa fa-phone"> Di động:</i>
                        </li>
                        <li>
                          <i class="fa fa-envelope"> Thư điện tử:</i>
                        </li>
                        <li class="m-top-xs">
                          <i class="fa fa-facebook"> Facebook:</i>
                          <a href="http://www.kimlabs.com/profile/" target="_blank"></a>
                        </li>
                      </ul>
                      

                      <br>

                      <!-- start skills -->
                      
                      <!-- end of skills -->

                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      
                      <!-- bắt đầu form danh sách công việc -->
                      <div class="row">
                      <div class="clearfix"></div>
                      <div class="clearfix"></div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Danh sách công việc</h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                   
                          <div class="x_content">
                            <div class="table-responsive">
                              <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                  <tr class="headings">
                                    <th>
                                      <div class="icheckbox_flat-green" style="position: relative;"><div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                    </th>
                                    <th class="column-title">Mã NV </th>
                                    <th class="column-title">Họ tên NV </th>
                                    <th class="column-title">Email</th>
                                    <th class="column-title">Ngày sinh </th>
                                    <th class="column-title">Điện thoại </th>
                                    <th class="column-title">Chức vụ </th>
                                    <th class="column-title no-link last"><span class="nobr"></span>
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                      <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                  </tr>
                                </thead>

                                <tbody>
                                  <tr class="even pointer">
                                    <td class="a-center ">
                                      <div class="icheckbox_flat-green" style="position: relative;"><div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                    </td>
                                    <td class=" ">121000040</td>
                                    <td class=" ">ABCXXXXXXXXXXXXXXX</td>
                                    <td class=" ">ABCXXXXXXXXXXXXXXXXXXXX@gmail.com </td>
                                    <td class=" ">John Blank L</td>
                                    <td class=" ">097XXXXXXX</td>
                                    <td class="a-right a-right ">Giám Đốc</td>
                                    <!-- <td class=" last"><a href="#">View</a> -->
                                    <td>
                                    <a href="http://localhost:8000/chi-tiet-nhan-vien" class="btn btn-primary btn-xs" style="width: 70px;"><i class="fa fa-eye"></i> Chi tiết </a>
                                    <a href="#" class="btn btn-danger btn-xs" style="width: 70px;"><i class="fa fa-trash-o"></i> Xóa </a>
                                  </td>
                                    
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                     <!-- Kết thúc form danh sách công việc -->                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection