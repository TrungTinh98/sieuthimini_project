<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Siêu Thị MiNi | Login </title>

  <!-- Bootstrap -->
  <link href="{{asset('../vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{asset('../vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{asset('../vendors/nprogress/nprogress.css')}}" rel="stylesheet">
  <!-- Animate.css -->
  <link href="{{asset('../vendors/animate.css/animate.min.css')}}" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="{{asset('../build/css/custom.min.css')}}" rel="stylesheet">

  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <!-- iCheck -->
  <script src="{{ asset('js/icheck.min.js') }}"></script>

  
</head>

<body class="login">
  <div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
      <div class="animate form login_form">
        <section class="login_content">
          <form method="POST" action="{{ route('login') }}">
            @csrf
            <h1 style="font:normal; font-size: 20px; "><b>Trang Đăng Nhập</b></h1>
            <div>
              <input name="id" type="text" class="form-control" placeholder="ID người dùng"  />
            </div>
            <div>
              <input name="password" type="password" class="form-control" placeholder="Mật khẩu" />
            </div>
            <div class="clearfix" style="color: #de5b5b;  font-size: 12px;" >
              <div class="form-group row">
                @if ($errors->has('id'))
                <label for="password" class="login-errors">{{ $errors->first('id') }}</label>
                @endif
              </div>
              <div class="form-group row">
                @if ($errors->has('password'))
                <label for="password" class="login-errors">{{ $errors->first('password') }}</label>
                @endif
              </div>
            </div>
            <div>
              <button type="submit" class="btn btn-primary submit" >Đăng nhập</button>
              <!-- <a class="btn btn-default submit" href="index.html">Đăng nhập</a> -->
            </div>

            

            <div class="separator">
              <p class="change_link">Quên mật khẩu?
                <a href="#signup" class="to_register"><u>Lấy lại mật khẩu</u></a>
              </p>

              <div class="clearfix"></div>
              <br />

              <div>
                <h1><i class="fa fa-paw"></i> Siêu Thị MiNi!</h1>
                <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
              </div>
            </div>
          </form>
        </section>
      </div>

      <div id="register" class="animate form registration_form">
        <section class="login_content">
          <form id="forgot"  name="confirm_email" action="{{url('doi-mat-khau')}}" method="post">
            @csrf
            <h1 style="font:normal; font-size: 20px; "><b>Lấy Lại Mật Khẩu</b></h1>

            <div>
              <input type="email" name="email"  id="input_email" placeholder="Địa chỉ email" class="form-control"/>
              <p id="pThongBao" style="color: #de5b5b"></p>
            </div>

            <div>
              <button type="submit" id="email"  class="btn btn-md btn-primary"> Xác Nhận <span class="glyphicon glyphicon-saved"></span>
              </button>
              <div class="form-group row" style="color: #de5b5b">
                @if ($errors->has('email'))
                <label for="email" class="login-errors">{{ $errors->first('email') }}</label>
                @endif
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="separator">
              <p class="change_link">Quay lại?
                <a href="{{url('/')}}" class="to_register"><u>Đăng nhập</u> </a>
              </p>

              <div class="clearfix"></div>
              <br />

              <div>
                <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
              </div>
            </div>
          </form>
        </section>
      </div>
    </div>
  </div>
</body>
</html>



<script type="text/javascript">
 $('#forgot').on("submit", function(e){  
    e.preventDefault();
    var data_test = $('#input_email').val();
    $.ajax({

        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"{{url('doi-mat-khau')}}",  
        method:"POST",  
        data:new FormData(this),  
        contentType:false,
        cache:false,
        processData:false, 
        success: function(data){   
            if(data.status == false) {
              document.getElementById("pThongBao").innerHTML =data.message;
              
            } else {               
               document.getElementById("pThongBao").innerHTML = "Đã xác nhận.Vui lòng kiểm tra gmail của bạn!";
            }
        },
        error: function(data) {
          console.log("error");
            console.log(data);
        }
    });
});
    
  // kết thúc đoạn ajax.
</script>
